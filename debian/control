Source: ksmtp
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               doxygen,
               graphviz,
               kf6-extra-cmake-modules,
               kf6-kcoreaddons-dev,
               kf6-ki18n-dev,
               kf6-kio-dev,
               kpim6-kmime-dev,
               libsasl2-dev,
               pkg-kde-tools-neon,
               qt6-base-dev
Standards-Version: 4.6.2
Homepage: https://projects.kde.org/projects/kde/pim/ksmtp
Vcs-Browser: https://code.launchpad.net/~kubuntu-packagers/kubuntu-packaging/+git/ksmtp
Vcs-Git: https://git.launchpad.net/~kubuntu-packagers/kubuntu-packaging/+git/ksmtp

Package: kpim6-ksmtp
Architecture: any
X-Neon-MergedPackage: true
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Replaces: libkpimsmtp6, libkpimsmtp5
Description: library for handling SMTP data
 This library provides an API for handling SMTP services.
 SMTP (Simple Mail Transfer Protocol) is the most prevalent
 Internet standard protocols for e-mail transmission.

Package: kpim6-ksmtp-dev
Architecture: any
X-Neon-MergedPackage: true
Multi-Arch: same
Depends: misc:Depends, shlibs:Depends
Depends: kf6-kcoreaddons-dev,
         kf6-kio-dev,
         kpim6-ksmtp (= ${binary:Version}),
         kpim6-kmime-dev,
         libboost-dev (>= 1.34.0~),
         libsasl2-dev,
         ${misc:Depends}
Replaces: libkpimsmtp-dev
Description: library for handling SMTP data
 This library provides an API for handling SMTP services.
 SMTP (Simple Mail Transfer Protocol) is the most prevalent
 Internet standard protocols for e-mail transmission.
 .
 This package contains the development files.

Package: libkpimsmtp6
Architecture: all
Depends: kpim6-ksmtp, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkpimsmtp5
Architecture: all
Depends: kpim6-ksmtp, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkpimsmtp-dev
Architecture: all
Depends: kpim6-ksmtp-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.
